const appirio = {
  sonarQubeUrl: "https://sonarqube.appirio.com"
};

module.exports = {
  source: {
    "**/config/*": true,
  },
  ingredients: [
    {
      "type": "list",
      "name": "enableSonarQube",
      "message": "Enable quality scanning using SonarQube?",
      "choices": ["Yes", "No"],
      filter: val => (val === "Yes")
    },
    {
      "type": "input",
      "name": "sonarUrl",
      "message": "What's the URL of your SonarQube instance?",
      when: answers => {
        return answers.enableSonarQube;
      },
      "default": appirio.sonarQubeUrl
    },
  ],
}
